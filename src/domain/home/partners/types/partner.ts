export interface Partner {
  name: string;
  icon: string;
  altLangIcons?: { [key: string]: string }; // Icons in other languages
}
